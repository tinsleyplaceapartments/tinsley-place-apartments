Welcome to Tinsley Place and Columbus Heights, two residential communities that feature one, two, and three bedroom apartments in Waco, TX. Spacious layouts and amenities welcome you home, along with exceptional service and an ideal location within walking distance to shopping.

Address: 715 Cleveland Ave, Waco, TX 76706, USA

Phone: 254-221-7852
